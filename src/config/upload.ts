import path from 'path';
import crypto from 'crypto';
import multer from 'multer';
import aws from 'aws-sdk';
import multerS3 from 'multer-s3';
import { hash } from 'bcryptjs';

const tmpFolder = path.resolve(__dirname, '..', '..', 'tmp');

const storageTypes = {
  local: multer.diskStorage({
    destination: tmpFolder,
    filename(request, file, callback) {
      const filehash = crypto.randomBytes(10).toString('hex');
      const fileName = `${filehash}-${file.originalname}`;

      callback(null, fileName);
    },
  }),
  s3: multerS3({
    s3: new aws.S3(),
    bucket: 'pedecerto.com',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    acl: 'public-read',
    key: (req, file, cb) => {
      crypto.randomBytes(16, (err, hash) => {
        if (err) cb(err);

        const filename = `${hash.toString('hex')}-${file.originalname}`;

        cb(null, filename);
      });
    },
  }),
};

export default {
  directory: tmpFolder,
  storage: storageTypes.s3,
};
