import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('points')
class Points {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  id_product: string;

  @Column()
  points: number;

  @Column()
  conversionPoints: number;

  @Column()
  value: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Points;
