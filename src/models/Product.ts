import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('products')
class Product {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  img_product: string;

  @Column()
  description: string;

  @Column()
  price: number;

  @Column()
  disponivel: boolean;

  @Column()
  id_user: string;

  @Column()
  id_category: string;

  @Column()
  id_sizeproduct: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Product;
