import { Router } from 'express';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

import UpdateProductService from '../modules/store/modules/products/services/EditProductService';
import CreateProductService from '../modules/store/modules/products/services/CreateProductsService';

const usersRouter = Router();

usersRouter.post('/product', async (request, response) => {
  const {
    name,
    description,
    price,
    disponivel,
    img_product,
    id_sizeproduct,
  } = request.body;
  const id_user = request.user.id;

  const createProduct = new CreateProductService();

  const product = await createProduct.execute({
    description,
    price,
    name,
    id_user,
    disponivel,
    img_product,
    id_sizeproduct,
  });

  return response.json(product);
});

usersRouter.put(
  '/product/:id',
  ensureAuthenticated,
  async (request, response) => {
    const {
      name,
      description,
      price,
      disponivel,
      img_product,
      id_sizeproduct,
    } = request.body;
    const id_user = request.user.id;
    const { id } = request.params;

    const updateProduct = new UpdateProductService();

    const product = await updateProduct.execute({
      id,
      description,
      disponivel,
      id_user,
      img_product,
      name,
      price,
    });

    return response.json(product);
  },
);

export default usersRouter;
