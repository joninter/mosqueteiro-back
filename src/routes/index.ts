import { Router } from 'express';

//Administrator
import usersRouter from './users.routes';
import sessionsRouter from './sessions.routes';

const routes = Router();

//Administrator
routes.use('/users', usersRouter);
routes.use('/login', sessionsRouter);
routes.use('/store', sessionsRouter);

export default routes;
