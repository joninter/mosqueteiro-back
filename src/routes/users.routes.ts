import { Router } from 'express';

import ensureAuthenticated from '../middlewares/ensureAuthenticated';

import UpdateUserService from '../modules/users/services/EditUserService';
import CreateUserService from '../modules/users/services/CreateUserService';

const usersRouter = Router();

usersRouter.post('/', async (request, response) => {
  const {
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    birth,
    pix,
  } = request.body;

  const createUser = new CreateUserService();

  const user = await createUser.execute({
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    birth,
    pix,
  });

  // Com a atualização do TypeScript, isso se faz necessário
  const userWithoutPassword = {
    id: user.id,
    name: user.name,
    email: user.email,
    role: user.role,
    address: user.address,
    created_at: user.created_at,
    updated_at: user.updated_at,
  };

  return response.json(userWithoutPassword);
});

usersRouter.post('/:code_indication', async (request, response) => {
  const {
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    birth,
    pix,
  } = request.body;
  const { code_indication } = request.params;

  const createUser = new CreateUserService();

  const user = await createUser.execute({
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    birth,
    code_indication,
    pix,
  });

  // Com a atualização do TypeScript, isso se faz necessário
  const userWithoutPassword = {
    id: user.id,
    name: user.name,
    email: user.email,
    role: user.role,
    address: user.address,
    created_at: user.created_at,
    updated_at: user.updated_at,
  };

  return response.json(userWithoutPassword);
});

usersRouter.put('/:id', ensureAuthenticated, async (request, response) => {
  const {
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    birth,
    pix,
  } = request.body;
  const { id } = request.params;

  const createUser = new UpdateUserService();

  const user = await createUser.execute({
    id,
    name,
    email,
    password,
    role,
    pix,
    address,
    phone,
    cpf,
    birth,
  });

  // Com a atualização do TypeScript, isso se faz necessário
  const userWithoutPassword = {
    id: user.id,
    name: user.name,
    email: user.email,
    role: user.role,
    cpf: user.cpf,
    birth: user.birth,
    address: user.address,
    created_at: user.created_at,
    updated_at: user.updated_at,
  };

  return response.json(userWithoutPassword);
});

usersRouter.patch('/:id', ensureAuthenticated, async (request, response) => {
  const { role } = request.body;
  const { id } = request.params;

  const createUser = new UpdateUserService();

  const user = await createUser.UpdateRole({ id, role });

  // Com a atualização do TypeScript, isso se faz necessário
  const userWithoutPassword = {
    id: user.id,
    name: user.name,
    email: user.email,
    role: user.role,
    cpf: user.cpf,
    birth: user.birth,
    address: user.address,
    created_at: user.created_at,
    updated_at: user.updated_at,
  };

  return response.json(userWithoutPassword);
});

export default usersRouter;
