import AppError from '../../../errors/AppError';

interface Request {
  name: string;
  email: string;
  password: string;
}

export class ValidatorRegister {
  public execute({ name, email, password }: Request): void {
    if (name === undefined || name === null || name === '') {
      throw new AppError('Error name not is empty', 400);
    }

    if (email === undefined || email === null || email === '') {
      throw new AppError('Error email not is empty', 400);
    }

    if (password === undefined || password === null || password === '') {
      throw new AppError('Error password not is empty', 400);
    }
  }
}
