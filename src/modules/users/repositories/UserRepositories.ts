import { getRepository } from 'typeorm';

import User from '../../../models/User';
import { CreateUser, EditUser, EditUserRole } from '../interfaces/User';

class UsersRepository {
  public async findByUser(email: string): Promise<User | null> {
    const usersRepository = getRepository(User);
    const findUser = await usersRepository.findOne({ where: { email } });

    return findUser || null;
  }

  public async createUser({
    name,
    email,
    password,
    role,
    address,
    phone,
    cpf,
    pix,
    birth,
    code_indication,
  }: CreateUser): Promise<User> {
    const usersRepository = getRepository(User);

    const user = usersRepository.create({
      name,
      email,
      password,
      role,
      address,
      phone,
      pix,
      cpf,
      birth,
      code_indication,
    });

    await usersRepository.save(user);

    return user;
  }

  public async updateUser({
    address,
    name,
    role,
    email,
    phone,
    cpf,
    birth,
    pix,
    id,
    password,
  }: EditUser): Promise<User> {
    const usersRepository = getRepository(User);
    return await usersRepository.save({
      address,
      name,
      role,
      email,
      phone,
      cpf,
      birth,
      pix,
      id,
      password,
    });
  }

  public async updateUserRole({ role, id }: EditUserRole): Promise<User> {
    const usersRepository = getRepository(User);
    return await usersRepository.save({ id, role });
  }
}

export default UsersRepository;
