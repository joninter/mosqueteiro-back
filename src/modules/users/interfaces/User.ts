export interface CreateUser {
  name: string;
  email: string;
  password: string;
  role: string;
  address: string;
  phone: string;
  cpf: string;
  pix: string;
  birth: Date;
  code_indication?: string;
}

export interface EditUser {
  name: string;
  email: string;
  password?: string;
  role: string;
  address: string;
  phone: string;
  cpf: string;
  birth: Date;
  pix: string;
  id: string;
}

export interface EditUserRole {
  role: string;
  id: string;
}
