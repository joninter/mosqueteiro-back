import { hash } from 'bcryptjs';

import AppError from '../../../errors/AppError';
import { ValidatorRegister } from '../errors/Validators';

import User from '../../../models/User';
import { EditUser, EditUserRole } from '../interfaces/User';
import UsersRepository from '../repositories/UserRepositories';

class EditUserService {
  public async execute({
    address,
    phone,
    cpf,
    birth,
    email,
    id,
    role,
    pix,
    password,
    name,
  }: EditUser): Promise<User> {
    const user = new UsersRepository();
    const validator = new ValidatorRegister();

    const checkUserExists = await user.findByUser(email);

    if (!checkUserExists) {
      throw new AppError('User not found', 406);
    }

    if (password === undefined || password === null) {
      password = checkUserExists.password;
    } else {
      const hashedPassword = await hash(password, 8);
      password = hashedPassword;
    }

    validator.execute({ name, email, password });

    const updatedUser = await user.updateUser({
      address,
      name,
      password,
      role,
      id,
      pix,
      email,
      birth,
      cpf,
      phone,
    });

    return updatedUser;
  }

  public async UpdateRole({ id, role }: EditUserRole): Promise<User> {
    const user = new UsersRepository();

    const UserUpdateRole = user.updateUserRole({ id, role });

    return UserUpdateRole;
  }
}

export default EditUserService;
