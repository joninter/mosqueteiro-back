import AppError from '../../../errors/AppError';

import UserRepositories from '../repositories/UserRepositories';

interface Request {
  id: string;
}

class DeleteUserService {
  public async execute({ id }: Request): Promise<void> {
    const userRepositories = new UserRepositories();

    const userExists = await userRepositories.findByUser(id);

    if (!userExists) {
      throw new AppError('User not exists');
    }

    await userRepositories.remove(userExists);
  }
}

export default DeleteUserService;
