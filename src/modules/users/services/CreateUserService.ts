import { hash } from 'bcryptjs';

import AppError from '../../../errors/AppError';
import { ValidatorRegister } from '../errors/Validators';

import User from '../../../models/User';
import { CreateUser } from '../interfaces/User';
import UsersRepository from '../repositories/UserRepositories';

class CreateUserService {
  public async execute({
    name,
    email,
    password,
    role,
    address,
    phone,
    pix,
    cpf,
    birth,
    code_indication,
  }: CreateUser): Promise<User> {
    const user = new UsersRepository();
    const validator = new ValidatorRegister();

    console.log('senha', password);

    validator.execute({ name, email, password });

    const checkUserExists = await user.findByUser(email);

    if (checkUserExists) {
      throw new AppError('Email address already used.', 406);
    }

    role === undefined || role === null ? (role = 'user') : (role = 'admin');

    const hashedPassword = await hash(password, 8);

    const newUser = await user.createUser({
      name,
      birth,
      cpf,
      phone,
      password: hashedPassword,
      email,
      address,
      pix,
      role,
      code_indication,
    });

    return newUser;
  }
}

export default CreateUserService;
