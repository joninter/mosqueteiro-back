import { getRepository } from 'typeorm';

import Invite from '../../../models/Invite';

class InvitesRepository {
  public async findByUser(id_user: string): Promise<Invite | null> {
    const usersRepository = getRepository(Invite);
    const findUser = await usersRepository.findOne({ where: { id_user } });

    return findUser || null;
  }

  public async createInvite({ id_user, codeInvite }: any): Promise<Invite> {
    const usersRepository = getRepository(Invite);
    let count = 0;

    count = 0;

    const user = usersRepository.create({
      codeInvite,
      count,
      id_user,
    });

    await usersRepository.save(user);

    return user;
  }
}

export default InvitesRepository;
