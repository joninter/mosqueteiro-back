import Invite from '../../../models/Invite';
import { CreateInvite } from '../interfaces/Invites';
import InvitesRepository from '../repositories/InviteRepository';

class CreateUserService {
  public async execute({ id_user, codeInvite }: CreateInvite): Promise<Invite> {
    const user = new InvitesRepository();

    const newUser = await user.createInvite({
      id_user,
      codeInvite,
    });

    return newUser;
  }
}

export default CreateUserService;
