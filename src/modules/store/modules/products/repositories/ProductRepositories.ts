import { getRepository } from 'typeorm';

import Product from '../../../../../models/Product';
import { CreateProduct, EditProduct } from '../interfaces/Product';

class ProductRepository {
  public async findByProduct(id: string): Promise<Product | null> {
    const productRepository = getRepository(Product);
    const findProduct = await productRepository.findOne({ where: { id } });

    return findProduct || null;
  }

  public async findAllProduct(): Promise<Product[]> {
    const productRepository = getRepository(Product);
    const findProduct = await productRepository.find();

    return findProduct;
  }

  public async findByProductsInCategory(
    id_category: string,
  ): Promise<Product[]> {
    const productRepository = getRepository(Product);
    const findProduct = await productRepository.find({
      where: { id_category },
    });

    return findProduct;
  }

  public async createProduct({
    name,
    description,
    id_sizeproduct,
    id_category,
    price,
    img_product,
    id_user,
    disponivel,
  }: CreateProduct): Promise<Product> {
    const productRepository = getRepository(Product);

    const product = productRepository.create({
      name,
      id_category,
      id_sizeproduct,
      description,
      disponivel,
      id_user,
      img_product,
      price,
    });

    await productRepository.save(product);

    return product;
  }

  public async updateProduct({
    description,
    id_sizeproduct,
    id_category,
    disponivel,
    id_user,
    img_product,
    price,
    name,
  }: EditProduct): Promise<Product> {
    const usersRepository = getRepository(Product);
    return await usersRepository.save({
      id_category,
      id_sizeproduct,
      price,
      img_product,
      id_user,
      disponivel,
      description,
      name,
    });
  }
}

export default ProductRepository;
