export interface CreateProduct {
  name: string;
  img_product: string;
  description: string;
  price: number;
  disponivel: boolean;
  id_user: string;
  id_category?: string;
  id_sizeproduct?: string;
}
export interface Request {
  id: string;
  name: string;
  img_product: string;
  description: string;
  price: number;
  disponivel: boolean;
  id_user: string;
  id_category?: string;
  id_sizeproduct?: string;
}

export interface EditProduct {
  id?: string;
  name: string;
  img_product: string;
  description: string;
  price: number;
  disponivel: boolean;
  id_user: string;
  id_category?: string;
  id_sizeproduct?: string;
}
