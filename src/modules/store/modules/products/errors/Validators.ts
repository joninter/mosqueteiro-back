import AppError from '../../../../../errors/AppError';

interface Request {
  name: string;
  img_product: string;
  description: string;
  price: number;
  disponivel: boolean;
  id_user: string;
}

export class ValidatorRegister {
  public execute({
    name,
    img_product,
    description,
    price,
    id_user,
    disponivel,
  }: Request): void {
    if (name === undefined || name === null || name === '') {
      throw new AppError('Error name not is empty', 400);
    }

    if (
      img_product === undefined ||
      img_product === null ||
      img_product === ''
    ) {
      throw new AppError('Error img not is empty', 400);
    }

    if (
      description === undefined ||
      description === null ||
      description === ''
    ) {
      throw new AppError('Error description not is empty', 400);
    }
    if (price === undefined || price === null || price < 0) {
      throw new AppError('Error points not is empty', 400);
    }
    if (disponivel === undefined || disponivel === null) {
      throw new AppError('Error disponivel not is empty', 400);
    }
  }
}
