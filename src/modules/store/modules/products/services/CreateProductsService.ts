import { ValidatorRegister } from '../errors/Validators';

import Product from '../../../../../models/Product';
import { CreateProduct } from '../interfaces/Product';
import ProductRepositories from '../repositories/ProductRepositories';

class CreateUserService {
  public async execute({
    description,
    name,
    disponivel,
    id_user,
    img_product,
    price,
    id_category,
    id_sizeproduct,
  }: CreateProduct): Promise<Product> {
    const product = new ProductRepositories();
    const validator = new ValidatorRegister();

    validator.execute({
      name,
      description,
      disponivel,
      id_user,
      price,
      img_product,

    });

    const newProduct = await product.createProduct({
      name,
      description,
      img_product,
      price,
      id_user,
      disponivel,
      id_category,
      id_sizeproduct,
    });

    return newProduct;
  }
}

export default CreateUserService;
