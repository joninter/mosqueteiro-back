import { hash } from 'bcryptjs';

import AppError from '../../../../../errors/AppError';
import { ValidatorRegister } from '../errors/Validators';

import Product from '../../../../../models/Product';
import { Request } from '../interfaces/Product';
import ProductRepositories from '../repositories/ProductRepositories';

class EditUserService {
  public async execute({
    description,
    disponivel,
    id_sizeproduct,
    id_category,
    id_user,
    price,
    img_product,
    name,
    id,
  }: Request): Promise<Product> {
    const product = new ProductRepositories();
    const validator = new ValidatorRegister();

    const checkProductExists = await product.findByProduct(id);

    if (!checkProductExists) {
      throw new AppError('Product not found', 404);
    }

    validator.execute({
      name,
      img_product,
      price,
      id_user,
      disponivel,
      description,
    });

    const updatedUser = await product.updateProduct({
      description,
      disponivel,
      id_user,
      price,
      img_product,
      name,
      id_category,
      id_sizeproduct,
    });

    return updatedUser;
  }
}

export default EditUserService;
