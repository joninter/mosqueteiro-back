import { getRepository } from 'typeorm';

import AppError from '../../../../../errors/AppError';

import Product from '../../../../../models/Product';

interface Request {
  id: string;
}

class DeleteProductService {
  public async execute({ id }: Request): Promise<void> {
    const productRepository = getRepository(Product);

    const productExists = await productRepository.findOne({
      where: { id },
    });

    if (!productExists) {
      throw new AppError('product not exists');
    }

    await productRepository.remove(productExists);
  }
}

export default DeleteProductService;
