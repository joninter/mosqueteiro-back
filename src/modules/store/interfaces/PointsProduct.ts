export interface CreatePointProduct {
  id_product: string;
  points: number;
}

export interface EditPointsProducts {
  id_product: string;
  points: number;
  id: string;
}
